# Makefile for publishing the Org project

EMACS = emacs
EMACSFLAGS = --batch --no-init-file --load publish.el --funcall org-publish-all
PROJECT_DIR = $(HOME)/git/jagrg.gitlab.io
PUBLIC_DIR = $(PROJECT_DIR)/public
TIMESTAMPS_DIR = $(HOME)/.org-timestamps

# Default target
all: publish

# Publish target
publish:
	@echo "Publishing project..."
	@if [ -z "$$(ls -A $(PUBLIC_DIR))" ]; then \
		echo "Public directory is empty."; \
		TIMESTAMPS_FLAG=nil; \
	else \
		echo "Public directory is not empty."; \
		TIMESTAMPS_FLAG=t; \
	fi; \
	echo "TIMESTAMPS_FLAG=$$TIMESTAMPS_FLAG"; \
	cd $(PROJECT_DIR) && env VISUAL="" TIMESTAMPS_FLAG=$$TIMESTAMPS_FLAG $(EMACS) $(EMACSFLAGS)

# Sitemap target
sitemap:
	@echo "Generating sitemap..."
	@npx sitemap-generator-cli -l https://jagrg.gitlab.io/ 2>/dev/null

# Clean target (optional)
clean:
	@echo "Cleaning up..."
	@cd $(PROJECT_DIR) && rm -rf public/*
	@rm -rf $(TIMESTAMPS_DIR)/*

# Help target
help:
	@echo "Makefile for publishing the Org project"
	@echo ""
	@echo "Usage:"
	@echo "  make          - Publish the project"
	@echo "  make sitemap  - Generate the sitemap"
	@echo "  make clean    - Clean the output directory"
	@echo "  make help     - Show this help message"
