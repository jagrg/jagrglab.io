;; publish.el --- Publish Org documents to HTML

;; Copyright (c) 2018-2024 Jonathan Gregory

;; This file is NOT part of GNU Emacs.

;; Code in this document is free software; you can redistribute it
;; and/or modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation, either version 3 of
;; the License, or (at your option) any later version.

;; This code is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;;; Commentary:

;;; Code:

(require 'package)
(package-initialize)

(defun site-local-p ()
  (file-exists-p "~/git/org-mode/lisp"))

(defun blog-post-p ()
  (let ((file buffer-file-name))
    (and file
         (string-match-p "/blog/.*$" (or (file-name-directory file) ""))
         (not (string-match-p "/blog/tag.*$" (file-name-directory file)))
         (not (string= (file-name-nondirectory file) "blog.org")))))

(unless (site-local-p)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (package-refresh-contents)
  (dolist (package '(htmlize lua-mode dash ox-rss))
    (unless (package-installed-p package)
      (package-install package))))

(require 'org)
(require 'dash)
(require 'ox-publish)
(require 'seq)
(require 'ox-rss)
;; (setq debug-on-error t)

(load-file "ox-extra.el")
(ox-extras-activate '(ignore-headlines))

(setq org-html-divs '((preamble "header" "preamble")
      		      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-head-include-default-style nil
      org-html-html5-fancy t
      org-html-htmlize-output-type 'css
      org-html-validation-link nil
      org-html-doctype "html5"
      org-html-scripts "")

(defun read-template-file (template)
  (let (results)
    (with-temp-buffer
      (insert-file-contents template)
      (setq results (buffer-substring-no-properties (point-min) (point-max))))
    results))

(setq org-html-head (read-template-file "./source/templates/head.html"))

(defvar site-navigation (read-template-file "./source/templates/nav.html"))

(setq org-html-postamble-format '(("en"  "")))

;; webMaster element is optional but required by the ox-rss library
(setq user-full-name "Jonathan Gregory"
      user-mail-address "jgrg@autistici.org")

(setq org-rss-image-url "https://jagrg.org/assets/logo.png")

;; let gitlab runner handle timestamps
(setq org-publish-use-timestamps-flag
      ;; Invalidate the cache if the public directory is empty
      (if (equal (getenv "TIMESTAMPS_FLAG") "nil")
          nil
        t))

(setq org-export-default-language "en"
      org-export-with-toc nil
      org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      ;; https://orgmode.org/manual/Exporting-Code-Blocks.html
      ;; org-export-use-babel nil
      org-export-with-drawers '(not "NOTES"))

;; stop *.html~ files from being created
(setq make-backup-files nil)

(defvar site-attachments
  (regexp-opt '("jpg" "JPG" "jpeg" "gif" "png" "svg" "mp3" "ogg" "js" "webmanifest"
                "json" "ico" "cur" "css" "js" "woff2" "ttf" "otf" "html" "pdf" "bib"))
  "File types that are published as static files.")

(defvar site-repository
  "https://gitlab.com/jagrg/jagrg.gitlab.io")

(defvar site-root
  (expand-file-name "."))

(defun site-blog-publish-sitemap (title list)
  "Publish the blog's site map file."
  (setcdr list (seq-filter
                (lambda (file)
                  (not (string-match "file:draft_" (car file))))
                (cdr list)))
  (mapconcat #'identity
	     (list (format (read-template-file "./source/templates/head-blog.org") title)
		   (org-list-to-subtree list))
	     "\n"))

(org-link-set-parameters
 "play"
 :export (lambda (path desc format)
           (cond
            ((eq format 'html)
             ;; Ensure link IDs are unique
             (let ((basename (file-name-nondirectory path)))
               (format (read-template-file "../templates/audio.html")
                       basename path (or desc path) basename path)))
            (t
             (format "%s" path)))))

(defun site-truncate-preview (file length)
  (let (beg end paragraph)
    (with-temp-buffer
      (insert-file-contents file)
      (save-excursion
        (goto-char (point-min))
        ;; remove links
        (while (re-search-forward "\\(\\[\\[.+]\\[\\(.+\\)]]\\)" nil t)
          (replace-match "\\2" nil nil))
        (goto-char (point-min))
        (when (re-search-forward
               "^[ \t]*#\\+name: preview[ \t]*$" nil t 1)
          (forward-line 1)
          (setq beg (point))
          (search-forward-regexp "[ \t]*\n$")
          (setq end (point))
          (setq paragraph (buffer-substring-no-properties beg end)))))
    (if (< (length paragraph) length)
        paragraph
      (concat (string-trim-right (substring paragraph 0 length))
              "..."))))

;; Remove date and links from post description
(defun remove-date-and-links (contents)
  (with-temp-buffer
    (insert contents)
    (goto-char (point-min))
    (let ((date-pattern "<p>\n[[:space:]]*\\([0-9]+\\) \\([A-Za-z]+\\) \\([0-9]+\\)[[:space:]]*\n</p>"))
      (while (re-search-forward date-pattern nil t)
        (replace-match "")))
    (let ((link-pattern " <a href=\"[^\"]+\">Read.+»</a>"))
      (while (re-search-forward link-pattern nil t)
        (replace-match "")))
    (buffer-string)))

(defun org-rss-headline--advice (orig-fun headline contents info)
  (let ((clean-contents (remove-date-and-links contents)))
    (funcall orig-fun headline clean-contents info)))

(advice-add 'org-rss-headline :around #'org-rss-headline--advice)

(defun site-blog-format-entry (entry style project)
  "Format the blog's main page."
  (when (not (directory-name-p entry))
    ;; To change the title the cache files in
    ;; ~/.org-timestamps may need to be cleared
    (let* ((title (org-publish-find-title entry project))
           (date (org-publish-find-date entry project))
           (pubdate (format-time-string
                     (car org-time-stamp-formats)
                     (org-publish-find-date entry project)))
           (postdate (format-time-string "%d %b %Y" date))
           (link (concat (file-name-sans-extension entry) ".html"))
           (preview (site-truncate-preview (concat "source/blog/" entry) 338)))
      (format "[[file:%s][%s]]
:PROPERTIES:
:PUBDATE: %s
:RSS_PERMALINK: %s
:RSS_TITLE: %s
:HTML_HEADLINE_CLASS: headline
:END:
#+begin_postdate
%s
#+end_postdate
%s [[file:%s][Read more »]]"
              entry title pubdate link title postdate preview entry))))

;; #+include: posts.org
(defun fetch-recent-posts (file &optional n)
  "Fetch the first N headings from FILE."
  (let (headings (counter 0))
    (with-current-buffer (find-file-noselect
                          (expand-file-name file))
      (goto-char (point-min))
      (while (and (< counter (or n 3))
                  (re-search-forward org-complex-heading-regexp nil t))
        (push (match-string-no-properties 4) headings)
        (setq counter (1+ counter))))
    (mapconcat (lambda (item)
                 (concat "- " item))
               (reverse headings)
               " \n")))

;; (setq org-html-extension "")

(defun write-post-to-file (content)
  (let ((file "./source/posts.org")
        (current-content ""))
    (when (file-readable-p file)
      (with-temp-buffer
        (insert-file-contents file)
        (setq current-content (buffer-string))))
    (unless (string= current-content content)
      (with-temp-file file
        (insert content)))))

(when (site-local-p)
  (write-post-to-file (fetch-recent-posts "./source/blog/blog.org")))

(defun site-git-file-tracked-p (file)
  "Return t if FILE is tracked."
  (not (string-equal
	(with-output-to-string
	  (with-current-buffer standard-output
	    (call-process "git" nil t nil "ls-files" "--no-error-unmatch" file)))
	"")))

(defun site-git-last-update-date (file)
  "Return the last commit date of FILE."
  (with-output-to-string
    (with-current-buffer standard-output
      (call-process "git" nil t nil "log" "-1" "--date=short" "--pretty=format:%cd" file))))

(defun site-blog-postamble (info)
  "Format post amble conditionally."
  (let* ((file (plist-get info :input-file))
	 ;; If file is tracked, return the date of the last commit.
	 ;; Otherwise return the date of when file was last modified.
	 (modtime (if (site-git-file-tracked-p file)
	 	      (format "<a href=\"%s/commits/master/%s\">%s</a>"
			      site-repository
			      (file-relative-name file site-root)
			      (site-git-last-update-date file))
		    (format-time-string "%F" (nth 5 (file-attributes file))))))
    (cond
     ((string= (car (plist-get info :title)) "Blog")
      (read-template-file "../templates/footer-blog.html"))
     ((string= (car (plist-get info :title)) "Filter by tags")
      "")
     (t (format (read-template-file "../templates/post-footer.html") modtime)))))

(defun site-postamble (info)
  (let ((file (plist-get info :input-file)))
    (when (string-match-p "index.org" file)
      (read-template-file "./templates/footer.html"))))

(defun blog-get-org-keyword (keyword)
  "Return the value of KEYWORD in the current buffer."
  (org-element-map (org-element-parse-buffer 'element) 'keyword
    (lambda (kw)
      (when (string= (org-element-property :key kw) keyword)
        (org-element-property :value kw)))
    nil t))

;; https://list.orgmode.org/CACJZKxiwiFpCSvYtovEBU=QpH0WF77k+EboemKwS2BqFWwpmyQ@mail.gmail.com/
;; https://git.savannah.gnu.org/cgit/emacs/org-mode.git/commit/?id=d38d53a17
(defun blog-html-head-extra ()
  "Set the Open Graph Protocol tags dynamically."
  (when (blog-post-p)
    (let* ((title (blog-get-org-keyword "TITLE"))
           (description (or (blog-get-org-keyword "DESCRIPTION") ""))
           (base-url "https://jagrg.org/")
           (filename (or buffer-file-name ""))
           (relative-filename (if (string= filename "") "" (file-relative-name filename)))
           (url (if (string= relative-filename "")
                    base-url
                  (concat base-url (file-name-sans-extension relative-filename) ".html"))))
      ;; Set og tags for blog posts only
      (when (not (string= filename ""))
        (setq org-html-head-extra
              (format (read-template-file "../templates/og.html")
                      title description url))))))

(defun blog-export-before-processing-hook (backend)
  "Hook to set up HTML head extra before exporting to HTML."
  (when (org-export-derived-backend-p backend 'html)
    (blog-html-head-extra)))

(add-hook 'org-export-before-processing-hook #'blog-export-before-processing-hook)
;; (add-hook 'org-export-before-processing-functions #'blog-export-before-processing-hook)

(defun blog-html-inject-aside ()
  (when (blog-post-p)
    (let ((timestamp (blog-get-org-keyword "DATE")))
      (when timestamp
        (let* ((base-url "https://jagrg.org/")
               (filename (when buffer-file-name (file-relative-name buffer-file-name)))
               (url (concat base-url (file-name-sans-extension filename) ".html"))
               (time (org-time-string-to-time timestamp))
               (date (format-time-string "%d %b %Y" time))
               (datetime (format-time-string "%Y-%m-%d 12:00:00" time))
               (tags (blog-get-org-keyword "FILETAGS")))
          (format (read-template-file "../templates/aside.html") datetime date url tags))))))

;; Filter by filetags
(defun blog-post-json-data ()
  (when (blog-post-p)
    (let* ((title (blog-get-org-keyword "TITLE"))
           (timestamp (blog-get-org-keyword "DATE"))
           (time (org-time-string-to-time timestamp))
           (date (format-time-string "%d %b %Y" time))
           (tags (or (blog-get-org-keyword "FILETAGS") ""))
           (summary (site-truncate-preview buffer-file-name 338))
           (clean-summary (replace-regexp-in-string "\n" " " summary))
           (html (org-export-string-as clean-summary 'html t))
           (html-summary (replace-regexp-in-string "<p>\\|</p>" "" html))
           (url (file-name-base buffer-file-name)))
        (unless (string-match "draft_" url)
          `(("title" . ,title)
            ("date" . ,date)
            ("tags" . ,tags)
            ("summary" . ,(replace-regexp-in-string "\n" " " html-summary))
            ("url" . ,url))))))

(defun blog-generate-posts-json (_)
  (let (data)
    (dolist (file (directory-files "./source/blog" 'full (rx ".org" eos)))
      (with-current-buffer (find-file-noselect file)
        (let ((json (blog-post-json-data)))
          (when json
            (push json data)))))
    (with-temp-file "./source/posts.json"
      (insert (json-encode-list data))
      (json-pretty-print-buffer))
    (json-encode-list data)))

;; First value is the class, second the tag and third the id?
;; Looks like only id can be modified:
;; :html-divs '((preamble "header" "preamble")
;;              (content "main" "content h-entry")
;;              (postamble "footer" "postamble"))
;; Gives the wrong output.
;; <main id="content h-entry" class="content">

;; https://indiewebify.me/validate-h-entry/
(defun org-html-add-h-entry-class (output backend info)
  (when (and (org-export-derived-backend-p backend 'html)
             (string-match-p (regexp-quote "/blog/")
                             (or (plist-get info :input-file) ""))
             (not (string= (car (plist-get info :title)) "Blog")))
    ;; (message "Initial output: %s" output)
    (let ((replacements
           '(("<main id=\"content\"\\(.+\\)?>" "<main id=\"content\" class=\"content h-entry\">")
             ("<h1 class=\"title\">" "<h1 class=\"title p-name\">"))))
      (dolist (replacement replacements output)
        (setq output (replace-regexp-in-string (car replacement) (cadr replacement) output)))
      (setq output (replace-regexp-in-string
                    "\\(<h1 class=\"title p-name\">.*?</h1>\\)"
                    (concat "\\1\n" (or (blog-html-inject-aside) ""))
                    output))
      output)))

(setq create-lockfiles nil)

;; (message "Filter functions: %s" org-export-filter-final-output-functions)
(add-to-list 'org-export-filter-final-output-functions 'org-html-add-h-entry-class)

(defun org-html-wrap-contents (contents backend info)
  "Wrap CONTENTS with <div class=\"e-content\"> if in blog/ directory."
  (when (and (org-export-derived-backend-p backend 'html)
             (string-match-p (regexp-quote "/blog/")
                             (or (plist-get info :input-file) ""))
             (not (string= (car (plist-get info :title)) "Blog")))
    (concat "\n<div class=\"e-content\">\n" contents "\n</div>")))

(add-to-list 'org-export-filter-body-functions 'org-html-wrap-contents)

(defun site-build-tag-links ()
  (let* (links
         (filetags (blog-get-org-keyword "FILETAGS"))
         (tags (when filetags (split-string filetags ", "))))
    (when tags
      (dolist (i tags)
        (push (format "[[file:tags?tag=%s][%s]]" i i) links))
      (concat "\n🏷️ " (mapconcat #'identity links ", ")))))

(defun site-append-tags (backend)
  (when (org-export-derived-backend-p backend 'html)
    (when (and buffer-file-name
               (blog-post-p))
      (goto-char (point-max))
      (insert (or (site-build-tag-links) "")))))

(add-to-list 'org-export-before-processing-hook 'site-append-tags)

(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "./source"
             :base-extension "org"
	     :publishing-function 'org-html-publish-to-html
	     :html-postamble 'site-postamble
             :html-preamble site-navigation
	     :publishing-directory "./public"
	     :html-head org-html-head
	     :exclude "README.org")
       (list "site-static"
             :base-directory "./source"
             :base-extension site-attachments
             :exclude (regexp-opt '("blog"))
             :recursive t
	     :publishing-directory "./public"
             :publishing-function 'org-publish-attachment)
       (list "blog-org"
	     :base-directory "./source/blog"
	     :base-extension "org"
	     :publishing-function 'org-html-publish-to-html
	     :html-head org-html-head
	     :html-postamble 'site-blog-postamble
	     :auto-sitemap t
             :html-preamble site-navigation
	     :sitemap-title "Blog"
	     :sitemap-filename "blog.org"
             :completion-function 'blog-generate-posts-json
	     :sitemap-function 'site-blog-publish-sitemap
	     :sitemap-format-entry 'site-blog-format-entry
	     :sitemap-sort-files 'anti-chronologically
	     :publishing-directory "./public")
       (list "blog-assets"
             :base-directory "./source/blog/assets"
             :base-extension site-attachments
	     :publishing-directory "./public/assets"
             :publishing-function 'org-publish-attachment)
       (list "blog-rss"
             :base-directory "./source/blog"
             :publishing-directory "./public"
             :base-extension "org"
             :html-link-home "https://jagrg.org/"
             :author "Jonathan Gregory"
             :description "Jonathan Gregory's blog"
             :html-link-use-abs-url t
             :rss-extension "xml"
             :publishing-function 'org-rss-publish-to-rss
             :exclude ".*"
             :include '("blog.org")
             :table-of-contents nil)
       (list "site" :components '("site-org" "site-static" "blog-org" "blog-assets" "blog-rss"))))

(provide 'publish)
;;; publish.el ends here
