#+title: Quantifying Achievements on a Resume
#+author: Jonathan Gregory
#+date: <2025-02-13 Thu>
#+filetags: resume, git
#+description: Learn how to quantify achievements on a resume using git
#+html_head_extra: <meta property="og:title" content="Quantifying Achievements on a Resume Using Git" />
#+html_head_extra: <meta property="og:description" content="Learn how to quantify achievements on a resume using Git" />
#+html_head_extra: <meta property="og:type" content="article" />
#+html_head_extra: <meta property="og:url" content="https://jagrg.org/2025-02-13_quantifying-achievements.html" />

#+name: preview
Using hard numbers on your resume provides clear, measurable proof of
your accomplishments, making your impact more tangible for employers.
If you want to stand out from other applicants, you need to focus on
what you achieved, not just your responsibilities. However, many
people (myself included), struggle with quantifying their work, so
let's explore some practical ways to do this using Git, the backbone
of modern software development.

** Tip #1: Percentage of commits you contributed

Don't just say you contributed to XYZ--that’s expected. Instead, focus
on how much you contributed and the impact of those contributions. For
example, to showcase your level of engagement, you could say something
like:

#+begin_quote
Authored 22% of all commits in XYZ, refactoring legacy components and
enhancing code maintainability.
#+end_quote

But how do we arrive at that 22%? Let's break it down.

Step 1: Find the total number of commits in the project

#+begin_src sh
git rev-list --count HEAD
# => 914
#+end_src

Step 2: Find how many of those commits are yours

#+begin_src sh
git rev-list --count --author="$(git config --global user.name)" HEAD
# => 208
#+end_src

Step 3: Calculate the percentage

: (208 / 914) * 100 = 22.76%

Now you should have a concrete metric to showcase your contributions.

** Tip #2: Measuring your code contributions

Tip #1 shows how active you were in a project, but how much actual
code did you contribute? One way is to count the total lines of code
in the project or in some relevant subdirectory (e.g., =src/= for
JavaScript developers):

Step 1: Count the total lines of code

#+begin_src sh
git ls-files src/ | xargs cat | wc -l
# => 32556
#+end_src

Step 2: Count the lines you modified

#+begin_src sh
git log --author="$(git config --global user.name)" \
    --pretty=tformat: --numstat -- src/ | \
    awk '{added+=$1; removed+=$2} END {print added+removed}'
# => 12822
#+end_src

Step 3: Calculate the percentage

: (12822 / 32556) * 100 = 39.38

Now you should have two complementary metrics to support your resume:

#+begin_quote
Authored 22% of all commits and modified 39% of the entire source
code, implementing key features and optimizing performance.
#+end_quote

This makes your impact clear and quantifiable.

** Tip #3: Number of commits in the last year

Inspired by GitHub’s [[https://github.com/IonicaBizau/git-stats][contribution graph]], another valuable metric is
the number of commits you've made in the past year. This is an
effective way to showcase your consistency and engagement in software
development.

If your projects are scattered across multiple directories (see
[[https://git.sr.ht/~jagrg/dotfiles/tree/master/item/common/bin/commits][this script]]) or in the same directory, you can sum up your total
commits across all repositories using:

#+begin_src sh
find ~/projects -mindepth 1 -maxdepth 2 -type d -name ".git" \
     -execdir git rev-list --count --since="1 year ago" \
     --author="$(git config --global user.name)" HEAD 2>/dev/null \; | \
    awk '{sum+=$1} END {print sum}'
# => 1655
#+end_src

What this does is it (1) finds all Git repositories inside the
~/projects/ folder, scanning up to 2 levels deep. (2) Then it counts
commits from the past year in each repository made by you, and (3) it
sums up the commit counts across all repositories.

If the output is =1655=, which is about four to five commits per day,
you could write something like:

#+begin_quote
Demonstrated ability to build [bla bla bla] with a total of =1655=
commits in the past year across multiple repositories, demonstrating
consistent contributions and iterative improvements.
#+end_quote

Does this sounds more impactful? I think it does!

** Final thoughts

By tracking your work, you provide concrete evidence of your
contributions, making your resume stand out to recruiters and hiring
managers. Have you tried quantifying your work like this? Do you use a
different strategy? Check out [[https://github.com/casperdcl/git-fame][git-fame]] for more insights and let’s
discuss! If you need a resume template, you're welcome to use [[https://sr.ht/~jagrg/tern-resume/][mine]].

:NOTES:
- https://github.com/IonicaBizau/git-stats
- https://github.com/ejwa/gitinspector
- https://github.com/jez/git-heatmap
:END:
