#+title: Parental controls
#+author: Jonathan Gregory
#+date: <2020-08-28 Fri>
#+filetags: programming
#+property: header-args :eval never-export

#+name: preview
I haven't put too much thought on [[https://en.wikipedia.org/wiki/Parental_controls][parental controls]], and I'm not sure
I fully agree with it. Either way, recently I began giving my
2-year-old one small dose of 30 flexible minutes of screen time using
[[https://mpv.io/][mpv]] and a few handmade Bash and Lua scripts.

The first step was to assemble playlist files and place them in a
"playlists" folder. Then I use a shell script for prompting the user
to pick one. When selected, the tracks are played randomly in
fullscreen with a clock and stopwatch on the top left corner of the
window.

To do this, I created both a Lua script file called ~track-time.lua~
in ~~/.config/mpv/scripts~ with the following lines:

#+begin_src lua
local opts = {
   show_time = "no",
}

local options = require 'mp.options'
options.read_options(opts)

if opts.show_time == "yes" then
   local secs = 0
local timer

   local function update_osd()
      local time = os.date(" %H:%M ")
      local elapsed = os.date('!%T', secs)
      mp.osd_message(elapsed .. time)
      secs = secs + 1
   end

   timer = mp.add_periodic_timer(1, update_osd)

   local function pause_timer(name, value)
      mp.add_timeout(0.1, function()
                        if value == true then
                           timer:stop()
                        else
                           timer:resume()
                        end
      end)
   end

   mp.observe_property("pause", "bool", pause_timer)
end
#+end_src

And an executable file called "playlist" with the following lines:

#+begin_src sh
#!/bin/bash

cd ~/playlists || exit

select file in $(find -- *); do
    mpv --script-opts=track_time-show_time=yes \
        --shuffle \
        --fs \
        --playlist="$file"
    exit
done
#+end_src

I'm actually using something more graphical for the sake of others who
are also using the program, but you get the idea. In any case, you can
open the playlist from the terminal or create a desktop application
for it.

Of course, playing outside and eating mud is still a better choice,
but if you need to introduce some screen time for your little one, and
assuming you know your way around Linux/UNIX, the solution presented
here may be a good place to start.
