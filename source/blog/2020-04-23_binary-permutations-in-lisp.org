#+title: Binary permutations in Lisp
#+author: Jonathan Gregory
#+date: <2020-04-23 Thu>
#+filetags: music programming
#+property: header-args :eval never-export

#+name: preview
In the book [[https://a.co/d/fXENnwu][The Elements of Rhythm Volume I]] rhythms are made using
binary strings of N bits, where each bit or binary digit represents
one of two values (0 and 1). This means there are 2 possible
combinations in 1 bit, 4 combinations in 2 bits and so on.

2^1 = 2\\
2^2 = 4\\
2^3 = 8\\
2^4 = 16\\
2^5 = 32\\
2^6 = 64\\
2^7 = 128\\
2^8 = 256

What follows is my attempt to generate all binary permutations of a
given length in Lisp. I know there's at least one function in the dash
library for working with permutations, but it won't work with binary
strings where the elements repeat. In other words, we need to ensure
that each element in the list is unique. To do this we create an index
of the pattern and transform not the binary but the indexed sequence,
which we revert later to its binary equivalent.

To begin we create a set of binary patterns of N elements. The first
pattern begins with zeros, then ones are added incrementally to all
subsequent patterns.

#+begin_src emacs-lisp
(defun pattern-create (bits)
  (let (seq (list (make-list bits 0)))
    (while (< (car (last list)) 1)
      (push (setq list (butlast (cl-list* 1 list)))
	    seq))
    (append (list (make-list bits 0))
	    (nreverse seq))))
;; => ((0 0 0) (1 0 0) (1 1 0) (1 1 1))
#+end_src

Then we rotate the pattern until all possible combinations are
exhausted.

#+begin_src emacs-lisp
(defun pattern-rotate (pattern)
  (let* (patterns
	 (map (--map-indexed (cons it-index it) pattern))
	 (index (mapcar #'car map)))
    (dolist (permutations (-permutations index))
      (push (mapcar (lambda (idx)
		      (assoc-default idx map))
		    permutations)
	    patterns))
    (cl-remove-duplicates patterns :test #'equal)))
;; => ((1 0 0) (0 1 0) (0 0 1))
#+end_src

Finally we iterate the functions to generate all possible combinations
of N bits:

#+begin_src emacs-lisp
(defun patterns (bits)
  (let (patterns)
    (dolist (i (pattern-create bits))
      (let ((permutation (nreverse (pattern-rotate i))))
	(setq patterns (append patterns permutation))))
    patterns))
;; => ((0 0 0) (1 0 0) (0 1 0) (0 0 1)...
#+end_src

So far the results are not sorted sequentially (0001, 0010, 0011, 0100
etc.) where the rightmost digits start over and the next digit is
incremented. This is easy to do, although I think shifting the pattern
to the right: 1000, 0100, 0010, 0001 etc. would be the most systematic
in terms of how rhythm exercises are usually organised. Either way
this works well for the purpose of the book as far as I can tell, but
the indexing process is expensive and slows things down as the bits
increase.

Another approach is to convert integers to base-2 strings using
[[https://www.gnu.org/software/emacs/manual/html_node/elisp/Bitwise-Operations.html][bitwise operations]]. Here's a refactored version of [[https://stackoverflow.com/questions/20568684/converting-number-to-base-2-binary-string-representation/20577329#20577329][this function]] to
make the conversions.

#+begin_src emacs-lisp
(defun integer-to-binary (integer)
  (let (list)
    (while (> integer 0)
      (setq list (append (list (if (= 1 (logand integer 1))
				   1 0))
			 (when list list)))
      (setq integer (lsh integer -1)))
    list))
#+end_src

And here's [[https://www.lispforum.com/viewtopic.php?t=688][another one]] using [[https://www.gnu.org/software/emacs/manual/html_node/elisp/Arithmetic-Operations.html][arithmetic operations]].

#+begin_src emacs-lisp
(defun integer-to-binary (integer)
  (cl-do ((integer integer (floor integer 2))
	  (list nil (cons (mod integer 2) list)))
      ((zerop integer) list)))
#+end_src

Finally we create a list of all binary combinations of a given length.

#+begin_src emacs-lisp
(defun patterns (bits)
  (let (list)
    (dotimes (i (expt 2 bits))
      (let ((binary (integer-to-binary i)))
	(push (append (make-list (- bits (length binary)) 0)
		      binary)
	      list)))
    (nreverse list)))
;; => ((0 0 0 0) (0 0 0 1) (0 0 1 0) (0 0 1 1) (0 1 0 0)...
#+end_src

This method is a lot faster compared to my first attempt, and works
well for the purpose of the book. Of course, the same can be done in
R, Python and possibly most other programming languages as well:

#+begin_src sh
R -e "expand.grid(0:1, 0:1, 0:1, 0:1)"
#+end_src

Still, it's always a lot more fun to think through these problems
yourself, especially when you begin to realise that 1 + 1 is actually
10!
