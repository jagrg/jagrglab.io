// Webmention

document.addEventListener('DOMContentLoaded', () => {
    const webmentionForm = document.getElementById('webmention-form');
    const targetInput = webmentionForm.querySelector('input[name="target"]');
    const currentUrl = window.location.href;

    targetInput.value = currentUrl;
});
