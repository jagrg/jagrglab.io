// Pagination

document.addEventListener("DOMContentLoaded", function () {
    const posts = document.querySelectorAll(".outline-2");
    const prevLink = document.querySelector(".pagination .prev");
    const nextLink = document.querySelector(".pagination .next");

    let currentPage = 1;
    const postsPerPage = 4; // Number of posts per page

    function showPage(page) {
        const startIndex = (page - 1) * postsPerPage;
        const endIndex = startIndex + postsPerPage;

        posts.forEach((post, index) => {
            if (index >= startIndex && index < endIndex) {
                post.style.display = "block";
            } else {
                post.style.display = "none";
            }
        });

        // Scroll to top when showing new page
        window.scrollTo({ top: 0, behavior: "smooth" });
    }

    prevLink.addEventListener("click", function (e) {
        e.preventDefault();
        if (currentPage > 1) {
            currentPage--;
            showPage(currentPage);
        }
    });

    nextLink.addEventListener("click", function (e) {
        e.preventDefault();
        const totalPosts = posts.length;
        const totalPages = Math.ceil(totalPosts / postsPerPage);

        if (currentPage < totalPages) {
            currentPage++;
            showPage(currentPage);
        }
    });

    // Initial call to show the first page
    showPage(currentPage);
});
