// Audio player

function initAudioPlayer(link, audio) {
  link.addEventListener("click", function(event) {
    event.preventDefault();
    if (audio.paused) {
      audio.play();
    } else {
      audio.pause();
    }
  });
}

document.addEventListener("DOMContentLoaded", function() {
  // Query all links with id matching pattern 'audioLink-<STR>'
  var links = document.querySelectorAll("[id^='audioLink-']");

  links.forEach(function(link) {
    // Extract the STR part from the link's id
    var hash = link.id.split('-')[1];
    var audio = document.getElementById('audio-' + hash);

    if (audio) {
      initAudioPlayer(link, audio);
    }
  });
});
