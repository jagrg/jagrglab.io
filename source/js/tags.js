// Filter content by tags (WIP)

document.addEventListener("DOMContentLoaded", function () {
    const tagList = document.getElementById("tag-list");
    const postList = document.getElementById("post-list");
    let currentTag = null; // Track the currently displayed tag

    fetch("./posts.json")
        .then((response) => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then((posts) => {
            const tags = new Set();
            posts.forEach((post) => {
                post.tags.split(",").forEach((tag) => {
                    tags.add(tag.trim());
                });
            });

            // Convert tags Set to Array for indexing
            const tagsArray = Array.from(tags);

            tagsArray.forEach((tag, index) => {
                const li = document.createElement("li");
                const a = document.createElement("a");
                a.href = `tags?tag=${tag}`;
                a.textContent = tag;
                a.dataset.tag = tag;
                a.addEventListener("click", (event) => {
                    event.preventDefault();
                    togglePostsByTag(tag, posts, event.target);
                });
                li.appendChild(a);

                // Append comma if not the last tag
                if (index < tagsArray.length - 1) {
                    const comma = document.createElement("span");
                    comma.textContent = ",";
                    li.appendChild(comma);
                }

                tagList.appendChild(li);
            });

            // Check URL for a tag to filter by
            const urlParams = new URLSearchParams(window.location.search);
            const initialTag = urlParams.get("tag");
            if (initialTag) {
                const tagLink = document.querySelector(
                    `a[data-tag="${initialTag}"]`
                );
                if (tagLink) {
                    tagLink.click();
                }
            }
        })
        .catch((error) => {
            console.error("Error fetching posts.json:", error);
        });

    function togglePostsByTag(tag, posts, clickedTag) {
        if (currentTag === tag) {
            // If the same tag is clicked again, hide the posts
            postList.innerHTML = "";
            currentTag = null;
            removeSelectedClass();
        } else {
            // Show posts for the clicked tag and hide posts for the previous tag
            const filteredPosts = posts.filter((post) => {
                const postTags = post.tags.split(",").map((t) => t.trim());
                return postTags.includes(tag);
            });
            displayPosts(filteredPosts);
            currentTag = tag;
            highlightTag(clickedTag);
        }
    }

    function displayPosts(posts) {
        postList.innerHTML = "";
        if (posts.length === 0) {
            const li = document.createElement("li");
            li.textContent = "No related posts found.";
            postList.appendChild(li);
        } else {
            posts.forEach((post) => {
                const li = document.createElement("li");

                const headline = document.createElement("h2");
                headline.className = "headline";
                const headlineLink = document.createElement("a");
                headlineLink.href = `${post.url}`;
                headlineLink.textContent = post.title;
                headline.appendChild(headlineLink);
                li.appendChild(headline);

                const postdate = document.createElement("div");
                postdate.className = "postdate";
                const postdateP = document.createElement("p");
                postdateP.textContent = post.date;
                postdate.appendChild(postdateP);
                li.appendChild(postdate);

                const summary = document.createElement("p");
                summary.innerHTML = `${post.summary} <a href="${post.url}">Read more »</a>`;
                li.appendChild(summary);

                postList.appendChild(li);
            });
        }
    }

    function highlightTag(selectedTag) {
        // Remove "selected" class from all tags
        const allTags = document.querySelectorAll("#tag-list a");
        allTags.forEach((tag) => {
            tag.classList.remove("selected");
        });

        // Add "selected" class to the clicked tag
        selectedTag.classList.add("selected");
    }

    function removeSelectedClass() {
        // Remove "selected" class from all tags
        const allTags = document.querySelectorAll("#tag-list a");
        allTags.forEach((tag) => {
            tag.classList.remove("selected");
        });
    }
});
