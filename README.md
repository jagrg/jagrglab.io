# jagrg.org

[![pipeline
status](https://gitlab.com/jagrg/jagrg.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/jagrg/jagrg.gitlab.io/commits/master)

The source code for [jagrg.org](https://jagrg.org/) is hosted on
[GitLab](https://gitlab.com/jagrg/jagrg.gitlab.io) and written in
Emacs Lisp. The website loads in [0.4
seconds](https://pagespeed.web.dev/analysis/https-jagrg-org/vfbdenaiox?form_factor=desktop)
and [weighs only 4KB](https://yellowlab.tools/result/h2xtxkfy7t). It
is featured in the exclusive [250KB
Club](https://250kb.club/jagrg-org/) and the [IndieWeb
Webring](https://xn--sr8hvo.ws/). You can interact with it by sending
[Webmentions](https://indieweb.org/Webmention).

## Installation

Clone the repository:

```bash
git clone https://gitlab.com/jagrg/jagrg.gitlab.io.git
cd jagrg.gitlab.io
```

Install dependencies:

```bash
yarn install
```

## Building the Application

```bash
make
```

## Running the Application

Start the development server:

```bash
yarn start
```

The app will be served at http://localhost:8000.

